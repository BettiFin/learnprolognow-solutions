% This program tests whether one list is a sublist of another:
append([],L,L).

append([H|T],L2,[H|L3]) :- append(T,L2,L3).

prefix(P,L) :- append(P,_,L).

suffix(S,L) :- append(_,S,L).

sublists(SubList,L) :- suffix(S,L), prefix(SubList,S).

% True: sublists([1,2],[3,1,2,3]).
% True: sublists([],[3,1,2,3]).
% False: sublists([4],[3,1,2,3]).
% [4|_] -> ... : sublists([4],Result).
% [4, _, 3, _|_] -> [_, 4, _, 3, _|_] -> ... : sublists([4,_,3,_],Result).
