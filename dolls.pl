% Program with transitivity: Can be used to check whether some entity is inside another entity (e.g., babuschkas).
is_directly_in(big_babuschka,biggest_babuschka).
is_directly_in(small_babuschka,big_babuschka).
is_directly_in(smallest_babuschka,small_babuschka).

inside(X,Y) :- is_directly_in(X,Y).

inside(X,Y) :- is_directly_in(X,Z),
           inside(Z,Y).

% True: inside(smallest_babuschka,biggest_babuschka).
% False: inside(big_babuschka,smallest_babuschka).
