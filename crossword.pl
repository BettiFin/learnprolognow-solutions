% Solves a crossword puzzle. When variables are passed as arguments, different solutions are returned.
word(astante,  a,s,t,a,n,t,e).
word(astoria,  a,s,t,o,r,i,a).
word(baratto,  b,a,r,a,t,t,o).
word(cobalto,  c,o,b,a,l,t,o).
word(pistola,  p,i,s,t,o,l,a).
word(statale,  s,t,a,t,a,l,e).

%crossword/6
crossword(V1,V2,V3,H1,H2,H3) :- word(V1,A,VH11,B,VH12,C,VH13,D),word(V2,E,VH21,F,VH22,G,VH23,H),word(V3,I,VH31,J,VH32,K,VH33,L),word(H1,M,VH11,N,VH21,O,VH31,P),word(H2,Q,VH12,R,VH22,S,VH32,T),word(H3,U,VH13,V,VH23,W,VH33,X), not(V1=V2),not(V1=V3),not(V1=H1),not(V1=H2),not(V1=H3),not(V2=V3),not(V2=H1),not(V2=H2),not(V2=H3),not(V3=H1),not(V3=H2),not(V3=H3),not(H1=H2),not(H1=H3),not(H2=H3).

% B: cobalto, D: astoria, E: baratto: crossword(astante,B,pistola,D,E,statale).