% Checks whether a given input is numeric (either an integer number or a rational).
input(1).
input(2.45).
input(abc).

is_numeric(X) :- input(X), number(X) ; rational(X).

% True: is_numeric(1).
% True: is_numeric(2.45).
% False: is_numeric(abc).
