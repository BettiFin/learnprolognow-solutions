% Computes the length of a list
len([],0).

len([_|Tail],Result) :- len(Tail,X), Result is X+1.

% 3: len([1,2,3],Result).
% len([],Result).
