% If two lists have the same length, the program will return True:
same_length([],[]).
same_length([X|Tx], [Y|Ty]) :- same_length(Tx,Ty).

% True: same_length([2,1],[1,2]).
% False: same_length([2],[1,2]).


