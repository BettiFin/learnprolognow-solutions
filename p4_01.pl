% 4.01 Write a predicate istree/1 which succeeds if and only if its argument
%      is a Prolog term representing a binary tree.
%
% istree(T) :- T is a term representing a binary tree (i), (o)

istree(nil).
istree(t(_,L,R)) :- istree(L), istree(R).


% Test cases (can be used for other binary tree problems as well)

% True: istree(t(a,t(b,t(d,nil,nil),t(e,nil,nil)),t(c,nil,t(f,t(g,nil,nil),nil)))).
% True: istree(t(a,nil,nil)).
% False: istree(t(a,nil)).
% True: istree(nil).
