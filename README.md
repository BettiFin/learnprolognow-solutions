# Learn Prolog Now! Solutions

This repository contains my personal solutions for some tasks of the Learn Prolog Now! online tutorial by Patrick Blackburn, Johan Bos, and Kristina Striegnitz.

Programs are named by the predicates they implement.

Link to the tutorial: [Webpage Learn Prolog Now!](https://www.let.rug.nl/bos/lpn/lpnpage.php?pageid=online)

It futher contains some programs from the P-99: Ninety-Nine Prolog Problems tutorial by Kalábovi.

Link to the Prolog problems: [Webpage P-99](https://kalabovi.org/pitel:flp:99pl)

Further interesting introductory material:

- lecture on Prolog arithmetics by Wiebke Petersen: [Slides](https://user.phil.hhu.de/~petersen/WiSe1819_Prolog/Petersen_Prolog_05.pdf)

Some code was taken from the official swipl Prolog documentation: [Sudoku example](https://www.swi-prolog.org/pldoc/man?section=clpfd-sudoku)

This repository contains further scripts I found useful while Prolog programming.

Bettina Finzel, 14.2.2024
