% Program that doubles the amount of entries by inserting every entry twice:
twice([],[]).

twice([X|Tx],[X|[X|Ty]]) :- twice(Tx,Ty).

% [1, 1, 2, 2]: twice([1,2],Result).
