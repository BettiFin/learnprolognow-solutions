% This program should return which way to go and which kind of vehicle to take. Not yet solved.
byCar(auckland,hamilton).
byCar(hamilton,raglan).
byCar(valmont,saarbruecken).
byCar(valmont,metz).

byTrain(metz,frankfurt).
byTrain(saarbruecken,frankfurt).
byTrain(metz,paris).
byTrain(saarbruecken,paris).

byPlane(frankfurt,bangkok).
byPlane(frankfurt,singapore).
byPlane(paris,losAngeles).
byPlane(bangkok,auckland).
byPlane(singapore,auckland).
byPlane(losAngeles,auckland).

go(X,Y,byCar) :- byCar(X,Y).
go(X,Y,byCar) :- byCar(X,Z), go(Z,Y,T).

go(X,Y,byTrain) :- byTrain(X,Y).
go(X,Y,byTrain) :- byTrain(X,Z), go(Z,Y,T).

go(X,Y,byPlane) :- byPlane(X,Y).
go(X,Y,byPlane) :- byPlane(X,Z), go(Z,Y,T).

travel(X,Y,T) :- go(X,Y,T).

travel(X,Y,T) :- go(X,Y,O), travel(Z,Y,T).

% byTrain: travel(metz,frankfurt,Result).
% frankfurt byTrain: travel(metz,Location,Result).
% byCar and more...: travel(hamilton,Location,Result).
