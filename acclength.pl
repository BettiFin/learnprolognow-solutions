% a tail recursive program
accLen([],A,A).

accLen([_|T],A,L)  :-    Anew  is  A+1,  accLen(T,Anew,L).

acclength(List,Length)  :-  accLen(List,0,Length).

% True: acclength([1,2,3],3).
% False: acclength([1,2,3],2).
