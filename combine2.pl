% combines two lists of same length by index of elements and puts the elements in lists.
combine2([],[],[]).

combine2([X|Tx],[Y|Ty],[[X,Y]|Z]) :- combine2(Tx,Ty,Z).

% [[1, 1], [2, 2], [3, 3]]: combine2([1,2,3],[1,2,3],Result).