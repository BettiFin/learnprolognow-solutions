% Illustrates how cut works, if you would like to see the "normal" process, remove the cut operator !.
% Combines the arguments of predicate i with those of predicate j.
% The cut operator stops the process after the unification of X with the first possible argument value of i.
% Query for s(X,Y).

s(X,Y):-  q(X,Y).

s(0,0).

q(X,Y):-  i(X), !, j(Y).

i(1).
i(2).

j(1).
j(2).
j(3).

% X:1, Y:1; X:1, Y:2; X:1, Y:3; X:0, Y:0: s(X,Y).
% Without cut: X:1, Y:1; X:1, Y:2; X:1, Y:3; X:2, Y:1; X:2 Y:2; X:2, Y:3; X:0, Y:0: s(X,Y).

