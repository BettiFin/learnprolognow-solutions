% Adds one to all elements of a list, unless the lists are empty, then it simply returns the empty list.
addone([],[]).

addone([H|Tx],[H2|Ty]) :- H2 is H+1, addone(Tx,Ty).

% []: addone([],Result).
% [2,3,4]: addone([1,2,3],Result).