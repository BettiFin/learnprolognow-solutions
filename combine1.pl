% combines two lists of same length by index of elements
combine1([],[],[]).

combine1([X|Tx],[Y|Ty],[X|[Y|Z]]) :- combine1(Tx,Ty,Z).

% [1, 1, 2, 2, 3, 3]: combine1([1,2,3],[1,2,3],Result).
