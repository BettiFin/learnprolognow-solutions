% Program that interacts with user
position(a,policeman).
position(b,guard).

find_position :-
    write("Whose position do you wish to know? Choose a or b."),nl,
    read(Input),nl,
    position(Input,Output),nl,
    write(Output).
% Start program by entering "find_position."
