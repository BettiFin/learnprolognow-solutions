% This is the more efficient predicate for reversing lists. It uses an
% accumulator.

accRev([],A,A).

accRev([H|T],A,R) :- accRev(T,[H|A],R).

reverse(L,R) :- accRev(L,[],R).

% [3, 2, 1]: reverse([1,2,3],R).
