% Program that stores results well formatted:
man(alan).
man(john).
man(george).

program :-
    open('resultsFormatted.txt',write, Stream),
    forall(man(Man), writeln(Stream,Man)),
    close(Stream).
