% combines two lists of same length by index of elements and makes the elements arguments of a predicate j.
j(X,Y) :- true.

combine3([],[],[]).

combine3([X|Tx],[Y|Ty],[j(X,Y)|Z]) :- combine3(Tx,Ty,Z).

% [j(1,1), j(2,2), j(3,3)]: combine3([1,2,3],[1,2,3],Result).
