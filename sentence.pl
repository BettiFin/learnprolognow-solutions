% This program generates sentences according to a pre-defined structure:
word(determiner,every).
word(determiner,some).
word(noun,student).
word(noun,'burger').
word(verb,hates).
word(verb,likes).

sentence(Word1,Word2,Word3,Word4,Word5):-
         word(determiner,Word1),
         word(noun,Word2),
         word(verb,Word3),
         word(determiner,Word4),
         word(noun,Word5).

% Ex. 1: [every student hates some burger]: sentence(A,B,C,D,E).
% Ex. 2: [every student likes every student]: sentence(A,B,likes,D,E).
