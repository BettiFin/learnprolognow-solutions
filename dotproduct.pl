% Program that computes the dot product of two vectors represented as lists. Dotproducts of empty lists are 0.
dotproduct([],[],0).
dotproduct([Hx|Tx],[Hy|Ty],Dotproduct) :- Product is Hx*Hy, dotproduct(Tx,Ty,ProductNew), Dotproduct is Product + ProductNew.

dot(Vector1, Vector2, Dotproduct) :- dotproduct(Vector1, Vector2, Dotproduct).

% True: dot([1,2,3],[3,2,1],10).
% False: dot([1,2,3],[1,2,1],10).
% 5: dot([1,2],[1,2],Result).
