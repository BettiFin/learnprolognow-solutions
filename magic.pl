% Magic beings
house_elf(dobby).
witch(hermione).

magic(X) :- house_elf(X).
magic(X) :- witch(X).

% True: magic(hermione).
