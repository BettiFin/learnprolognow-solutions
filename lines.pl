% Program that computes whether two points mark the ends of a horizontal or vertical line given their coordinates.
coord(a,0,0).
coord(b,0,1).
coord(c,1,0).
coord(d,1,1).

points(ab,a,b).
points(ac,a,c).
points(ad,a,d).
points(bc,b,c).
points(bd,b,d).
points(cd,c,d).

equal(C,D) :- C = D.

horizontal_line(W) :- points(W,A,B), coord(A,X,Y), coord(B,U,V), equal(Y,V).
vertical_line(Z) :- points(Z,A,B), coord(A,X,Y), coord(B,U,V), equal(X,U).

% True: vertical_line(ab).
% False: horizontal_line(ab).
