% This programs checks for inconsistency with respect to the RCC-8 calculus
dc(a,b).
ec(a,b).
ec(b,c).
po(f,b).
pp(b,e).

inconsistency(X,Y) :- dc(X,Y), ec(X,Y).

% True: inconsistency(a,b).
% False: inconsistency(f,b).
