% Finds the maximum value in a list for positive AND negative values,
% The accumulator is simply initialised to the head of a list.

accMax([],Max,Max).

accMax([H|T],Acc,Max) :-
	H > Acc,
	accMax(T,H,Max).

accMax([H|T],Acc,Max) :-
	H =< Acc,
	accMax(T,Acc,Max).
	
max([H|T],Max) :-
	accMax(T,H,Max).

% 3: max([1,3,2],Max).
% False: max([],Max).
