% Computes whether a person is jealous of another given background knowledge about persons:
loves(vincent,mia).
loves(marcellus,mia).

jealous(A,B) :- loves(A,C), loves(B,C).

% True: jealous(vincent,marcellus).
