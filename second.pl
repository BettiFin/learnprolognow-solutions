% Program that checks whether an entry is the second entry of a list:
second(X,[_|[X|_]]).

% True: second(2,[1,2,3]).
% True: second([],[[],[]]).
% False: second([],[1]).