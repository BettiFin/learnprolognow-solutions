% Reverts a list. Prone to infinite loop.

append([],L,L).

append([H|T],L2,[H|L3]) :- append(T,L2,L3).

naivereverse([],[]).

naivereverse([H|T],R):-  append(RevT,[H],R), naivereverse(T,RevT).

% [] naivereverse([],Result).
% [1,2]: naivereverse([2,1],Result).
