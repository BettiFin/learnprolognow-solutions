% Program that computes whether a person is an uncle based on parental and sibling relations and gender.
male(aldo).
male(bob).
male(charlie).
male(frank).
male(hank).
male(johannes).

parent(bob,aldo).
parent(bob,dora).
parent(emilia,aldo).
parent(emilia,dora).
parent(hank,irina).
parent(gabrielle,irina).
parent(hank,johannes).
parent(gabrielle,johannes).

sibling(dora,aldo).
sibling(aldo,dora).
sibling(bob,frank).
sibling(frank,bob).
sibling(hank,emilia).
sibling(emilia,hank).
sibling(bob,katja).
sibling(katja,bob).
sibling(irina,johannes).
sibling(johannes,irina).

married(bob,emilia).
married(emilia,bob).
married(gabrielle,hank).
married(hank,gabrielle).

uncle(A,B) :- sibling(A,C), parent(C,B), male(A).

uncle(A,B) :- male(A), married(A,C), sibling(C,D), parent(D,B).

% True: uncle(bob,irina).
% False: uncle(bob,frank).
