% Appends a list at the end of another list.
append([],L,L).

append([H|T],L2,[H|L3]) :- append(T,L2,L3).

% []: append([],[],Result).
% [1,2,3,1,2,3]: append([1,2,3],[1,2,3],Result).
