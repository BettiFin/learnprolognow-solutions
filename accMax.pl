% finds the maximum number in a list. Initialise A to zero in your
% query (applies to the base case, if we work with positive numbers
% only)! See also extended program called max.
accMax([H|T],A,Max)  :-
         H  >  A,
         accMax(T,H,Max).

accMax([H|T],A,Max)  :-
         H  =<  A,
         accMax(T,A,Max).

accMax([],A,A).

% 3: accMax([3,2,1],0,Max).

% 1: accMax([1,1,1],0,Max).

% 0: accMax([],0,Max).

% 2: accMax([1,1,1],2,Max). Does not make much sense though.
