% Program that tells you whether one location can be reached by train from another location:
directTrain(saarbruecken,dudweiler).
directTrain(forbach,saarbruecken).
directTrain(freyming,forbach).
directTrain(stAvold,freyming).
directTrain(fahlquemont,stAvold).
directTrain(metz,fahlquemont).
directTrain(nancy,metz).

travelFromTo(X,Y) :- directTrain(X,Y).

travelFromTo(Y,X) :- directTrain(X,Y).

travelFromTo(X,Y) :- directTrain(X,Z),
    travelFromTo(Z,Y).

travelFromTo(Y,X) :- directTrain(X,Z),
    travelFromTo(Z,Y).

% True: travelFromTo(saarbruecken,metz).
% Endless number of possible destinations due to circle: travelFromTo(metz,Result).

