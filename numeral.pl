% Formally returns successors of 0, where the number of successors equals the number of redos:
numeral(0).

numeral(succ(X)):- numeral(X).

% 0: numeral(X).