% Program that tells you whether one location can be reached by car, train or plane from another location:
byCar(auckland,hamilton).
byCar(hamilton,raglan).
byCar(valmont,saarbruecken).
byCar(valmont,metz).

byTrain(metz,frankfurt).
byTrain(saarbruecken,frankfurt).
byTrain(metz,paris).
byTrain(saarbruecken,paris).

byPlane(frankfurt,bangkok).
byPlane(frankfurt,singapore).
byPlane(paris,losAngeles).
byPlane(bangkok,auckland).
byPlane(singapore,auckland).
byPlane(losAngeles,auckland).

travel(X,Y) :- byCar(X,Y).
travel(X,Y) :- byTrain(X,Y).
travel(X,Y) :- byPlane(X,Y).
travel(Y,X) :- byCar(X,Y).
travel(X,Y) :- byTrain(X,Y).
travel(X,Y) :- byPlane(X,Y).

travel(X,Y) :- byCar(X,Z),
    travel(Z,Y).
travel(Y,X) :- byCar(X,Z),
    travel(Z,Y).
travel(X,Y) :- byTrain(X,Z),
    travel(Z,Y).
travel(Y,X) :- byTrain(X,Z),
    travel(Z,Y).
travel(X,Y) :- byPlane(X,Z),
    travel(Z,Y).
travel(Y,X) :- byPlane(X,Z),
    travel(Z,Y).

% True: travel(metz,auckland).
% Endless number of possible destinations due to circle: travel(metz,Result).
