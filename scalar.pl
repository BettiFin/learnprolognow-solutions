% Program that multiplies scalars (integer with list):
scalarMult(Integer,[],[]).

scalarMult(Integer,[H|Tx],[H2|Ty]) :- H2 is H*Integer, scalarMult(Integer,Tx,Ty).

% [2,4,6]: scalarMult(2,[1,2,3],Result).
