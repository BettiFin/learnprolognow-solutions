% If we have two complementary clauses, we can use the cut operator to ensure that Prolog tries only one of them.
% Here, we compare two numerical arguments to choose the maximum value.

max(X,Y,Y)  :-  X  =<  Y,!.
max(X,Y,X)  :-  X>Y.

% 4: max(4,1,Result).
% 4: max(4,4,Result).
% 1: max(1,2,Result).
