% swaps a binary tree
swap(leaf(X),Y) :- Y=leaf(X).

swap(tree(X,Y),Z) :- swap(X,XY), swap(Y,YY), Z=tree(YY,XY).

% tree(leaf(4),tree(leaf(2),leaf(1))): swap(tree(tree(leaf(1), leaf(2)), leaf(4)), T).
