% checks whether a term provided as argument is a complex term.
complexterm(X):-
         nonvar(X),
         functor(X,_,A),
         A  >  0.

% True: complexterm(j(3,3)).
% False: complexterm(j).
