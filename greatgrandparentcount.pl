% Program that computes whether a person is a greatgrandparent and for that the number of generations of this relation.
parent(pedro, hugo).
parent(hugo, herminia).
parent(herminia, ze).
parent(ze, quim).
parent(quim, tostas).
parent(tostas, faneca).
parent(faneca, xico).

grandparent(X,Y) :- parent(X,Z), parent(Z,Y).
greatcount(X,Y,0) :- parent(X,Y); grandparent(X,Y).
greatcount(X,Y,N1) :- parent(X,Z), greatcount(Z,Y,N2), N1 is N2+1.

% 2: greatcount(pedro,quim,Result). meaning that Pedro is the great-great-grandparent of Quim
% 0: greatcount(pedro,herminia,Result). meaning that Pedro is either a parent or a grandparent of Herminia